TEMPLATE = app
TARGET = qml-battery
QT += quick
SOURCES = main.cpp
HEADERS += stub.h

#app.files = \
#    $$files(*.qml) \
#    images

target.path = $$[QT_INSTALL_EXAMPLES]/sysinfo/qml-battery
app.path = $$[QT_INSTALL_EXAMPLES]/sysinfo/qml-battery
INSTALLS += target app


RESOURCES += \
    qml-battery.qrc
