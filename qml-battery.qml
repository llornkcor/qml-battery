/****************************************************************************
**
** Copyright (C) 2012 Digia Plc and/or its subsidiary(-ies).
** Copyright (C) 2018 Lorn Potter <lorn.potter@gmail.com>
** Contact: http://www.qt-project.org/legal
**
** This file is part of the QtSystems module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Digia.  For licensing terms and
** conditions see http://qt.digia.com/licensing.  For further information
** use the contact form at http://qt.digia.com/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Digia gives you certain additional
** rights.  These rights are described in the Digia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

import QtQuick 2.0
import QtSystemInfo 5.0
import QtQuick.Particles 2.11

Rectangle {
    id: app
    width: 1080
    height: 1920
    color: "black"
    property int speed: level2Speed(batinfo.level);
    property bool hasBattery: (batinfo.batteryStatus != -1)
    property string energyUnit: "mAh"

    MouseArea {
        anchors.fill: parent
        onClicked: {
            Qt.quit();
        }
    }

    BatteryInfo {
        id: batinfo;
        property string oldstate;

        onLevelChanged: {
            leveltext.text = "Level: " + level + "%"
        }

        onChargerTypeChanged:  {
            if (type == BatteryInfo.Unknown) {
                chargerType.text = "Unknown Charger"
            }else if (type == BatteryInfo.WallCharger) {
                chargerType.text = "Wall Charger"
            } else if (type == BatteryInfo.USBCharger) {
                chargerType.text = "USB Charger"
            } else if (type == BatteryInfo.VariableCurrentCharger) {
                chargerType.text = "Variable Current Charger"
            }
        }

        onChargingStateChanged: {
            if (state == BatteryInfo.UnknownChargingState) {
                chargeState.text = "Charging Unknown"
                if (batinfo.chargerType !=  BatteryInfo.Unknown) {
                    img.state = "Battery"
                    batinfo.oldstate = img.state;
                } else {
                    img.state = "WallPower"
                    batinfo.oldstate = img.state;
                }
            } else if (state == BatteryInfo.IdleChargingState) {
                chargeState.text = "Idle Charging"
            } else if (state == BatteryInfo.Charging) {
                chargeState.text = "Charging"
                img.state = "Charging"
                batinfo.oldstate = img.state;
            } else if (state == BatteryInfo.Discharging) {
                chargeState.text = "Discharging"
                img.state = "Battery"
                batinfo.oldstate = img.state;
            }
        }
        onLevelStatusChanged: {
            if (levelStatus == BatteryInfo.UnknownChargingState) {
                batStat.text = "Battery Unknown"
            }else if (levelStatus == BatteryInfo.Empty) {
                batStat.text = "Battery Empty"
            } else if (levelStatus == BatteryInfo.LevelLow) {
                batStat.text = "Battery Low"
                img.width = 20; img.height = 20;
            } else if (levelStatus == BatteryInfo.Ok) {
                batStat.text = "Battery Ok"
            } else if (levelStatus == BatteryInfo.LevelFull) {
                batStat.text = "Battery Full"
            }
        }
        onRemainingCapacityChanged: {
            remainingCapacity.text = "Remaiing capacity: " + capacity + " "+ energyUnit
            doBatteryLevelChange(level)
        }
        onRemainingChargingTimeChanged: { chargeTime.text = "Time to full: "
                                          + minutesToFull() +" minutes"; }

        onCurrentFlowChanged: {
            curFlow.text = "Current Energy: "+ flow +" mA";
          }
        onVoltageChanged: {
          voltageText.text = "Voltage: "+ voltage +" mV";
        }
        onHealthChanged: {
            if (health == BatteryInfo.HealthOk)
                healthState.text = "Health Ok"
            else if (health == BatteryInfo.HealthBad)
                healthState.text = "Health Bad"
            else
                healthState.text = "Health Unknown"
        }
        property alias batState : batinfo.chargingState

        Component.onCompleted: {
            onLevelChanged(batinfo.level)
            onChargerTypeChanged(batinfo.chargerType)
            onCurrentFlowChanged(batinfo.currentFlow)
            onRemainingCapacityChanged(batinfo.remainingCapacity)
            onRemainingChargingTimeChanged(batinfo.remainingChargingTime)
            onVoltageChanged(batinfo.voltage)
            onChargingStateChanged(batinfo.chargingState)
            onLevelStatusChanged(batinfo.levelStatus)
            maximum.text = "Total Capacity: "+ batinfo.maximumCapacity +" "+ energyUnit
            onHealthChanged(batinfo.health)
            getPowerState();
        }

    }

    function minutesToFull() {
        if (batinfo.remainingChargingTime > 0) {
            return (batinfo.remainingChargingTime/60.00)
        }
        return 0;
    }

    function level2Speed(level) {
        if (level > 90) {
            return 1000;
        } else if (level > 70) {
            return 1500;
        } else if (level > 60) {
            return 2000;
        } else if (level > 50) {
            return 2500;
        } else if (level > 40) {
            return 3000;
        } else if (level > 10) {
            return 3500;
        } else if (level < 11) {
            return 4000;
        }
    }

    function doBatteryLevelChange(level) {
        floorParticles.pulse(batinfo.level);
        batinfo.oldstate = img.state;
    //    img.state = "levelchange"
        //img.state = batinfo.oldstate;
        getPowerState();
    }

    function getPowerState() {

    }
    Column {
        anchors.centerIn: parent
        anchors.margins: 10

        Text {
            id: leveltext
            color: "white"
        }

        Text {
            id: voltageText
            color: "white"
        }

        Text {
            id: maximum
            color: "white"
        }

        Text {
            id: remainingCapacity
            color: "white"
        }

        Text {
            id: chargeTime
            color: "white"
        }

        Text {
            id: curFlow
            color: "white"
        }

        Text {
            id: batStat
            color: "white"
        }

        Text {
            id: chargerType
            color: "white"
        }

        Text {
            id: chargeState
            color: "white"
        }
        Text {
            id: healthState
            color: "white"
        }
    }

    function particleState() {
        if (img.state == "Battery") {
            particles.burst(50);
        }
    }

/////////////////////////
    ParticleSystem {
        id: particleSystem
        anchors.fill: parent

        ImageParticle {
            source: "images/blueStar.png"
            colorVariation: 0.1
            alpha: 0
            width: 1;//batinfo.level;
            height: 1;//batinfo.level;

//            Gravity {
//                angle: 90
//                magnitude: 32
//            }
        }
    }

    Emitter {
        id: floorParticles
        anchors { horizontalCenter: parent.horizontalCenter; }
        y: app.height
        width: 10
        height: 10
        lifeSpan: 3000
        enabled: false
        velocity: AngleDirection{magnitude: 270; angleVariation: 360}
        acceleration: PointDirection {xVariation: -60; yVariation: 10;}
        //     angle: 270
        //   angleDeviation: 45
        //    velocity: 50
        velocityFromMovement: 60
        emitRate: batinfo.level
        system: particleSystem
    }
    Image {
        id: img
        source: "images/blueStone.png"
        smooth: true
        width: batinfo.level; height: batinfo.level;

        ParticleSystem {
            id: particleSystem2
            anchors.fill: parent

            //        Gravity {
            //            angle: 45
            //            magnitude: 32
            //        }

            Emitter {
                id: particles
                width: 1;
                height: 1;
                y: parent.height
                anchors.centerIn: parent;
                emitRate: 400;
                enabled: true
                lifeSpan: 700; lifeSpanVariation: 300;
                system: particleSystem
                velocity: AngleDirection{magnitude: 135; angleVariation: 180}

                //          angle: 0;
                //          angleDeviation: 360;
                //          velocity: 100;
                velocityFromMovement: 30;
            }
        }

        states: [
            State {
                name: "WallPower"
                when: batinfo.type == BatteryInfo.WallCharger
                StateChangeScript { script: particles.burst(50); }
                PropertyChanges {
                    target: img; opacity: 1; source : "images/blueStone.png";
                    anchors.horizontalCenter: undefined
                    y: 0;  x: (app.width / 2) - (img.width / 2)
                }
                PropertyChanges { target: floorParticles; emitRate: 0; enabled: false; }

            },
            State {
                name: "Charging"
                when: batinfo.chargingState == BatteryInfo.Charging
                StateChangeScript { script: particles.burst(50); }
                PropertyChanges { target: img; y:app.height
                }
                PropertyChanges {
                    target: img; opacity: 1; source : "images/yellowStone.png";
                    anchors.horizontalCenter: parent.horizontalCenter;
                }
                PropertyChanges { target: floorParticles; emitRate: 0; enabled: false; }
            },

            State {
                name: "Battery"
                when: batinfo.chargingState == BatteryInfo.Discharging
                StateChangeScript { script: particles.burst(50); }
                PropertyChanges {
                    target: img; source : "images/redStone.png";
                    anchors.horizontalCenter: parent.horizontalCenter;
                }
                PropertyChanges { target: floorParticles; emitRate: batinfo.level; enabled: true; }
            },

            State {
                name: "levelchange"
                PropertyChanges {
                    target: yAnim
                    running: false;
                }
                PropertyChanges {
                    target: bubblebounceanim
                    from: app.height
                    to: app.height - (app.height * (batinfo.level / 100 ))
                }
                PropertyChanges {
                    target: yAnim
                    running: true;
                }
            }
        ]

        transitions: [
            Transition {
                from: "*"
                to: "WallPower"
                NumberAnimation{ property: "y"; to: 0; duration: 750; easing.type: Easing.InOutQuad; }
            },
            Transition {
                from: "WallPower"
                to: "*"
                NumberAnimation{ property: "y"; to: app.height; duration: 2000; easing.type: Easing.InOutQuad; }
            }
        ]

        SequentialAnimation on y {
            id: yAnim
            loops: Animation.Infinite
            running: img.state != "WallPower"
            NumberAnimation {
                id: bubblebounceanim;
                from: app.height; to: app.height - (app.height * (batinfo.level / 100 ))
                easing.type: Easing.OutBounce; duration: speed
            }
            ScriptAction { script: particleState() }
            PauseAnimation { duration: 750 }
        }

        SequentialAnimation on x {
            running: img.state == "WallPower"
            loops: Animation.Infinite
            id: xanim
            NumberAnimation { target: img; property: "x"; to: app.width - img.width; duration: 1500;
                easing.type: Easing.InOutQuad;  }
            NumberAnimation { target: img; property: "x"; to: 0; duration: 1500;
                easing.type: Easing.InOutQuad;}
        }
    }
}
